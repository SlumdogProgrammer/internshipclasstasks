package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Kitchen> kitchenList = new ArrayList<>();
        kitchenList.add(new HomeKithcen());
        kitchenList.add(new RestoranKitchen());

        for (Kitchen k : kitchenList)
            k.cleanFridge();
    }
}
